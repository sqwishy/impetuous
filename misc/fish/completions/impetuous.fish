set -l __fish_im_all_subcommands show s l summary ss ll doing d edit suggest import-legacy config-edit post encode decode a an repl

## pre thingy options
complete -c im -x -l log -a 'DEBUG INFO WARNING ERROR'
complete -c im -x -l since -d 'Operate on entries starting after ...'
complete -c im -x -l until -d 'Operate on entries ending before ...'
complete -c im -f -s y -l yesterday
complete -c im -f -s h -l help -d 'Shows something unhelpful at you.'

## doing
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a doing -d "Do something. Or don't!"
complete -c im -n '__fish_seen_subcommand_from doing' \
    -f -a '(im suggest -l=30)'
complete -c im -n '__fish_seen_subcommand_from d' \
    -f -a '(im suggest -l=30)'

## ... everything else
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a show -d "A list things."
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a summary -d "Submission breakdown."
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a edit -d "Edit some entries in $EDITOR."
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a post -d "Submit submissions."
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a import-legacy
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a config-edit
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a encode
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a decode
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a a
complete -c im -n "not __fish_seen_subcommand_from $__fish_im_all_subcommands" \
    -f -a an
