import logging
from datetime import datetime, timedelta
from textwrap import dedent

import attr
import pytest
from sqlalchemy.exc import IntegrityError

from impetuous.interaction import (Comparison, Field, Gather, Interaction,
                                   Param, impetuous_access_control)
from impetuous.legacy import load_legacy_sheet
from impetuous.sheet import Entry, Submission, not_loaded
from tests import now

logger = logging.getLogger(__name__)


def test_read_legacy():
    doc = dedent(
        """\
        !sheet
        links: {}
        units:
        - !unit
            end: 2018-01-18 21:06:27.976677-08:00
            start: 2018-01-18 21:06:22.535574-08:00
            task: hi
        - !unit
            end: 2018-01-18 21:06:31.473065-08:00
            start: 2018-01-18 21:06:27.976677-08:00
            task: yay
        - !unit
            end: 2018-01-18 21:08:13.900328-08:00
            start: 2018-01-18 21:06:31.473065-08:00
            task: bananas
            post_results:
              jira:
                MEME-1: "It works!"
        - !unit
            end: 2018-01-18 22:57:33.604387-08:00
            start: 2018-01-18 21:20:13.900328-08:00
            task: MEME-2
            comments:
              - foobar
        """
    )
    entries = load_legacy_sheet(doc)
    assert ['hi', 'yay', 'bananas', 'MEME-2'] ==\
        [e.text for e in entries]
    assert entries[-1].comment == "foobar"
    assert entries[-2].submissions == {
        ("jira", "MEME-1"): Submission(
            ext="jira",
            key="MEME-1",
            result="It works!",
        ),
    }


def test_find_results(conn, gen, agent):
    id, = gen.ins_entry(text='potatos', start=now)
    gather = Gather(
        fields=(Field('entry', 'text'), Field('entry', 'start'), Field('entry', 'end')),
        match=Comparison(Field('entry', 'id'), 'eq', Param('id')),
    )
    i = Interaction(conn, agent, impetuous_access_control)
    with conn.begin():
        res, = i.find(gather=gather, using={"id": id}, map_values=tuple)
        assert res == ('potatos', now, None)

        res, = i.find(gather=gather, using={"id": id}, unpack_items=Entry.joker)
        assert res.id == not_loaded
        assert res.text == 'potatos'
        assert res.start == now
        assert res.end == None
        assert res.comment == not_loaded


### silly benchmarks ...

@attr.s
class Meme(object):
    a = attr.ib(default=None)
    b = attr.ib(default=None)
    c = attr.ib(default=None)
    d = attr.ib(default=None)
    e = attr.ib(default=None)
    f = attr.ib(default=None)

all_source = [('a', 0), ('b', 1), ('c', 2), ('d', 3), ('e', 4), ('f', 5)]

def load_with_dict(src):
    return Meme(**dict(src))


def load_with_setattr(src):
    meme = Meme()
    for k, v in src:
        setattr(meme, k, v)
    return meme


def test_load_values(benchmark):
    values = [v for _, v in all_source]
    res = benchmark(lambda: Meme(*values))
    assert res == Meme(a=0, b=1, c=2, d=3, e=4, f=5)


@pytest.mark.parametrize("fnc", [load_with_dict, load_with_setattr], ids=["dict", "setattr"])
@pytest.mark.parametrize("src", [all_source, all_source[:1]], ids=["big", "small"])
def test_load_with(benchmark, fnc, src):
    benchmark(fnc, src)
