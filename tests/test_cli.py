import contextlib
import datetime
import unittest.mock

import pytz

from impetuous.cli import get_terminal_width
from impetuous.im import Impetuous

utc = pytz.utc
now = datetime.datetime(2010, 5, 4, 16, 0, 0, tzinfo=utc)


@contextlib.contextmanager
def expect_call(monkey, args=(), kwargs={}, and_return=None):
    prev = monkey.side_effect
    was_called = False
    def sub(*got_args, **got_kwargs):
        nonlocal was_called
        was_called = True
        monkey.side_effect = prev
        assert args == got_args
        assert kwargs == got_kwargs
        return and_return
    monkey.side_effect = sub
    yield
    assert was_called


def test_get_terminal_width_stty():
    with unittest.mock.patch('subprocess.check_output') as check_output:
        with expect_call(check_output, args=(['stty', 'size'],), and_return=b'123 119\n'):
            assert 119 == get_terminal_width()


def test_get_terminal_width_tput():
    with unittest.mock.patch('subprocess.check_output') as check_output:
        with expect_call(check_output, args=(['tput', 'cols'],), and_return=b'119\n'), \
             expect_call(check_output, args=(['stty', 'size'],), and_return=b'something-dumb\n'):
            assert 119 == get_terminal_width()


def test_get_terminal_width_echo():
    with unittest.mock.patch('subprocess.check_output') as check_output:
        with expect_call(check_output, args=('echo -n $COLUMNS',), kwargs={'shell': True}, and_return=b'119'), \
             expect_call(check_output, args=(['tput', 'cols'],), and_return=b'banana\n'), \
             expect_call(check_output, args=(['stty', 'size'],), and_return=b'something-dumb\n'):
            assert 119 == get_terminal_width()
